# MyLearning
What I have learned

* Learn_matplotlib：参考一些博客，做个matplotlib绘图的简单入门，以后再慢慢补充。 
* Learn_perl：关于Perl的简单入门。
  
* .OUTDATE：过时的东西，以后可能不再关注，仅作留恋
    * JDBC：WebIDE+Java+MySQL学习JDBC的基本操作，相关笔记见<https://idzc.me/tag/JDBC/>
    * LearnPythonTheHardWay：Python一个入门书吧，敲了代码，仍懵逼中...
    * SQL Server Lab：《数据库原理与设计》课的实验课记录的代码。事实证明，我low到常打开github查看函数是如何创建的等等。
    * OpenCV: 参考《OpenCV计算机视觉编程攻略（第三版）》，做个初级入门。
    * ComputerGraphics: 计算机图像学实验，使用vs2017+easyx完成。
